const globals = require("../config/globals");
const {
    getUsersQuery,
    getUserByEmailQuery,
    getUserByIdQuery,
    getAddUserQuery,
    getIsUserExistQuery, getUpdateUserQueryAndParams
} = require('./sql/user');

const getIsUserExistHandler = async ({schema, email, id}) => {
    const query = getIsUserExistQuery({schema, email, id});
    const result = await globals.pg.client.query(query, [email, id]);
    return result?.rowCount > 0;

};


const getUserByEmailHandler = async ({schema, email}) => {
    const query = getUserByEmailQuery({schema, email});
    const result = await globals.pg.client.query(query, [email]);
    return result?.rows || [];

};

const getAllUsersHandler = async ({schema, email}) => {
    if (email) return await getUserByEmailHandler({schema, email});
    else {
        const query = getUsersQuery({schema});
        const result = await globals.pg.client.query(query);
        return result?.rows || [];
    }
};

const getUserByIdHandler = async ({schema, userId}) => {
    const query = getUserByIdQuery({schema});
    const result = await globals.pg.client.query(query, [userId]);
    return result?.rows || [];
};

const addUserHandler = async ({schema, id, firstName, lastName, phone, email, userType}) => {
    const currentTime = Math.floor(Date.now() / 1000);
    const values = [currentTime, currentTime, id, firstName, lastName, phone, email, userType];
    const query = getAddUserQuery({schema});
    const result = await globals.pg.client.query(query, values);
    return {rowCount: result?.rowCount || 0, addedUser: result?.rows[0]};
};

const updateUserHandler = async (req) => {
    const queryData = getUpdateUserQueryAndParams(req);
    const result = await globals.pg.client.query(queryData.query, [...queryData.params]);
    return {rowCount: result?.rowCount || 0, updatedUser: result?.rows[0]};

};

module.exports = {getAllUsersHandler, getUserByEmailHandler, getUserByIdHandler, addUserHandler, getIsUserExistHandler, updateUserHandler};