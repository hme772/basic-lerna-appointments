const globals = require("../config/globals");
const {getAppointmentsQuery, getUpdateAppointmentQueryAndParams, addAppointmentQuery} = require("./sql/appointment");


const getAllAppointmentsHandler = async ({schema}) => {
    const query = getAppointmentsQuery(schema);
    const result = await globals.pg.client.query(query);
    return result?.rows || [];
};

const getAllAppointmentsByUserIdHandler = async ({schema, userId}) => {
    const query = getAppointmentsQuery(schema, true);
    const result = await globals.pg.client.query(query, [userId]);
    return result?.rows || [];
};

const updateAppointmentHandler = async (req) => {
    const queryData = getUpdateAppointmentQueryAndParams(req);
    const result = await globals.pg.client.query(queryData.query, [...queryData.params]);
    return result?.rowCount || 0;

};

const addAppointmentHandler = async ({
                                         schema,
                                         worker_id,
                                         client_id,
                                         appointment_type,
                                         subject,
                                         start_time,
                                         end_time,
                                         description
                                     }) => {

    const values = [Math.floor(Date.now() / 1000), worker_id, client_id, appointment_type, subject, start_time, end_time, description];
    const query = addAppointmentQuery({schema});
    const result = await globals.pg.client.query(query, values);
    return {rowCount: result?.rowCount || 0, ...result.rows[0]};
};

module.exports = {
    getAllAppointmentsHandler,
    getAllAppointmentsByUserIdHandler,
    updateAppointmentHandler,
    addAppointmentHandler
};
