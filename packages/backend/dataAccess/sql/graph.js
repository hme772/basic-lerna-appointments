const getAppointmentsByTypesQuery = ({schema}) => `
        SELECT type, COUNT(*) AS count
        FROM ${schema}."appointments" AS a
        JOIN ${schema}."appointment_type" AS at ON a.appointment_type = at.id
        WHERE a.is_active = true
        GROUP BY type;`
const getAppointmentsByTypesForClientsQuery = ({schema}) => `
        SELECT 
            u.first_name || ' ' || u.last_name AS client_name,
            at.type,
            COUNT(*) AS count
        FROM 
            ${schema}."appointments" AS a
        JOIN 
            ${schema}."users" AS u ON a.client_id = u.id
        JOIN 
            ${schema}."appointment_type" AS at ON a.appointment_type = at.id
        WHERE 
            a.is_active = true AND a.start_time < extract(epoch FROM current_date)
        GROUP BY 
            u.first_name, u.last_name, at.type
        ORDER BY 
            u.first_name, u.last_name, at.type
    `;

module.exports = {getAppointmentsByTypesQuery, getAppointmentsByTypesForClientsQuery};