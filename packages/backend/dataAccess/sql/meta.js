const getUserTypesQuery = ({schema}) => `SELECT * FROM ${schema || 'appointments'}.user_type;`;
const getAppointmentTypesQuery = ({schema}) => `SELECT * FROM ${schema || 'appointments'}.appointment_type;`;

module.exports = {getUserTypesQuery, getAppointmentTypesQuery};
