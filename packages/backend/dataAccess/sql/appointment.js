const {getUpdateQueryAndParams} = require("./updateService");

const getAppointmentsQuery = (schema, isByUserId=false) => {
    return `SELECT 
    a.id AS appointment_id,
    at.type AS type,
    a.description,
    a.client_id,
    a.worker_id,
    u_worker.first_name AS worker_first_name,
    u_worker.last_name AS worker_last_name,
    u_client.first_name AS client_first_name,
    u_client.last_name AS client_last_name,
    a.subject,
    to_timestamp(a.start_time) AS start_time,
    to_timestamp(a.end_time) AS end_time,
    a.is_active
FROM 
${schema || 'appointments'}.appointments a
JOIN 
    ${schema|| 'appointments'}.users u_worker ON a.worker_id = u_worker.id
JOIN 
${schema || 'appointments'}.users u_client ON a.client_id = u_client.id
JOIN 
${schema || 'appointments'}.appointment_type at ON a.appointment_type = at.id 
WHERE 
a.is_active=true${isByUserId ? `\n AND (a.worker_id = $1 OR a.client_id = $1)` : ";"}`
};

const getUpdateAppointmentQueryAndParams = (req) => {
    return getUpdateQueryAndParams(req,'appointments')
};

const addAppointmentQuery = ({schema}) => `
            INSERT INTO ${schema || 'appointments'}.appointments 
            (creation_time, worker_id, client_id, appointment_type, subject, start_time, end_time, description)
            VALUES
            ($1, $2, $3, $4, $5, $6, $7, $8)
            RETURNING id;
        `;

module.exports = {getAppointmentsQuery, getUpdateAppointmentQueryAndParams, addAppointmentQuery};