const {getUserTypesQuery, getAppointmentTypesQuery} = require('./sql/meta');
const globals = require("../config/globals");

const getAllUserAndAppointmentTypesHandler = async ({schema}) => {
    const userTypesQuery = getUserTypesQuery({schema});
    const userTypesResult = await globals.pg.client.query(userTypesQuery);
    const query = getAppointmentTypesQuery({schema});
    const result = await globals.pg.client.query(query);
    return {userTypes: userTypesResult?.rows || [], appointmentTypes: result?.rows || []};
};

module.exports = {getAllUserAndAppointmentTypesHandler};