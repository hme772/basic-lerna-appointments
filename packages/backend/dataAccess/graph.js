const globals = require("../config/globals");
const {getAppointmentsByTypesQuery, getAppointmentsByTypesForClientsQuery} = require("./sql/graph");

const getAppointmentsByTypesHandler = async ({schema}) => {
    const query = getAppointmentsByTypesQuery({schema});
    const result = await globals.pg.client.query(query);
    return result?.rows;
};

const getAppointmentsByTypesForClientsHandler = async ({schema}) => {
    const query = getAppointmentsByTypesForClientsQuery({schema});
    const result = await globals.pg.client.query(query);
    return result?.rows;
};


module.exports = {getAppointmentsByTypesHandler, getAppointmentsByTypesForClientsHandler};
