const Joi = require('joi');
const {validationFunctionsObj} = require('./validationService');

const validateGetAllAppointments = Joi.object({
    body: Joi.object({
        schema: validationFunctionsObj.schema,
    }),
    params: validationFunctionsObj.empty,
    query: validationFunctionsObj.empty,
});

const validateGetAllAppointmentsByUserId = Joi.object({
    body: Joi.object({
        schema: validationFunctionsObj.schema,
    }),
    params: Joi.object({
        userId: validationFunctionsObj.id,
    }),
    query: validationFunctionsObj.empty,
});

const validateUpdateAppointment = Joi.object({
    body: Joi.object({
        schema: validationFunctionsObj.schema,
        worker_id: validationFunctionsObj.optional_id,
        client_id: validationFunctionsObj.optional_id,
        appointment_type: validationFunctionsObj.optional_appointment_type,
        subject: validationFunctionsObj.optional_subject,
        start_time: validationFunctionsObj.optional_start_time,
        end_time: validationFunctionsObj.optional_end_time,
        description: validationFunctionsObj.optional_empty_string_allowed,
        is_active: validationFunctionsObj.optional_is_active,
    }).custom((value, helpers) => {
        if (Object.keys(value).length === 0) {
            return helpers.error('object.empty');
        }
        return value;
    }),
    params: Joi.object({
        id: validationFunctionsObj.uuid,
    }),
    query: validationFunctionsObj.empty,
});

const validateAddAppointment = Joi.object({
    body: Joi.object({
        schema: validationFunctionsObj.schema,
        worker_id: validationFunctionsObj.id,
        client_id: validationFunctionsObj.id,
        appointment_type: validationFunctionsObj.appointment_type,
        subject: validationFunctionsObj.subject,
        start_time: validationFunctionsObj.start_time,
        end_time: validationFunctionsObj.end_time,
        description: validationFunctionsObj.empty_string_allowed,
    }),
    params: validationFunctionsObj.empty,
    query: validationFunctionsObj.empty,
});

module.exports = {
    validateGetAllAppointments,
    validateGetAllAppointmentsByUserId,
    validateUpdateAppointment,
    validateAddAppointment,
};