const Joi = require("joi");

const validationFunctionsObj = {
    schema: Joi.string().trim().not(null, '').optional(),
    optional_id: Joi.number().integer().min(100000000).max(999999999).not(null, '').optional(),
    optional_email: Joi.string().trim().not(null, '').email().optional(),
    optional_subject: Joi.string().trim().optional().not(null, ''),
    optional_start_time : Joi.number().integer().optional().min(0),
    optional_end_time: Joi.number().integer().optional().min(0).greater(Joi.ref('start_time')),
    optional_appointment_type: Joi.number().integer().optional().min(1),
    optional_empty_string_allowed: Joi.string().trim().allow(null,'').optional(),
    optional_is_active: Joi.boolean().not(null).optional(),
    optional_name: Joi.string().trim().not(null, '').min(3).optional(),
    optional_phone: Joi.string().trim().not(null, '').length(10).optional(),
    optional_userType : Joi.number().integer().min(1).optional(),
    id: Joi.number().integer().min(100000000).max(999999999).not(null, '').required(),
    email: Joi.string().trim().not(null, '').email().required(),
    phone: Joi.string().trim().not(null, '').required().length(10),
    name: Joi.string().trim().required().not(null, '').min(3),
    userType : Joi.number().integer().required().min(1),
    empty: Joi.object(),
    empty_string_allowed: Joi.string().trim().allow(null,'').required(),
    subject: Joi.string().trim().required().not(null, ''),
    start_time : Joi.number().integer().required().min(0),
    end_time: Joi.number().integer().required().min(0).greater(Joi.ref('start_time')),
    appointment_type: Joi.number().integer().required().min(1),
    uuid: Joi.string().guid({ version: 'uuidv4' }).required(),
}

const validateSchemaOnly = Joi.object({
    body: Joi.object({
        schema: validationFunctionsObj.schema,
    }),
    params: validationFunctionsObj.empty,
    query: validationFunctionsObj.empty,
});

module.exports = {validationFunctionsObj, validateSchemaOnly};