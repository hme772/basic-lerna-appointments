const {validateSchemaOnly} = require('./validationService');

const validateGetAllGraphData = validateSchemaOnly;

module.exports = validateGetAllGraphData;