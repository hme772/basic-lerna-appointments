const dotenv = require('dotenv');
dotenv.config();

module.exports = {
	postgres: {
		isConnect: process.env.POSTGRES_CONNECT ? process.env.POSTGRES_CONNECT.trim().toLowerCase() === 'true' : true,
		connect: {
			user: process.env.POSTGRES_USER || 'postgres',
			host: process.env.POSTGRES_HOST || 'localhost',
			database: process.env.POSTGRES_DATABASE || 'postgres',
			password: process.env.POSTGRES_PASSWORD || 'postgres',
			port: process.env.POSTGRES_PORT ? parseInt(process.env.POSTGRES_PORT) : 5432
		},
		reconnectTime: 15 //seconds
	},

};