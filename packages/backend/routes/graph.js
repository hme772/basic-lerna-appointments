const express = require("express");
const {getAllGraphData} = require("../controls/graph");
const auth = require('../middlewares/auth');
const validateGetAllGraphData = require("../validations/graph");
const validateResource = require('../middlewares/validateResource');

const router = express.Router();
router.get("/", auth.protect, validateResource(validateGetAllGraphData), getAllGraphData);

module.exports = router;
