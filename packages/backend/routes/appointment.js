const express = require('express');
const {getAllAppointments, getAllAppointmentsByUserId, updateAppointment, addAppointment} = require("../controls/appointment");
const auth = require('../middlewares/auth');
const {
    validateGetAllAppointments,
    validateGetAllAppointmentsByUserId,
    validateUpdateAppointment,
    validateAddAppointment,
} = require('../validations/appointment');
const validateResource = require('../middlewares/validateResource');


const router = express.Router();
router.get("/", auth.protect, validateResource(validateGetAllAppointments), getAllAppointments);
router.get("/:userId", auth.protect, validateResource(validateGetAllAppointmentsByUserId), getAllAppointmentsByUserId);
router.put("/:id", auth.protect, validateResource(validateUpdateAppointment), updateAppointment);
router.post("/", auth.protect, validateResource(validateAddAppointment), addAppointment);

module.exports = router;