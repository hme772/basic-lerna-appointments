const {getUserTypeEntity, getUserEntity} = require("./entities/user");
const {getAppointmentTypeEntity, getAppointmentEntity} = require("./entities/appointment");

const createUserTypeTable = async (client, schema = 'temp') => {
    const createQuery = `
		CREATE SCHEMA IF NOT EXISTS ${schema};
		CREATE TABLE IF NOT EXISTS ${schema}."user_type"(
			${getUserTypeEntity()}
            )
            WITH (
                    OIDS = FALSE
            )
            TABLESPACE pg_default;
    `;

    const fillTableQuery = `
        INSERT INTO ${schema}."user_type" (id, type)
        VALUES
            (2,'worker'),
            (1,'client')
    `;

    await client.query(createQuery);
    const result = await client.query(`SELECT COUNT(*) FROM ${schema}."user_type"`);
    const rowCount = parseInt(result.rows[0].count);

    if (rowCount === 0) {
      await client.query(fillTableQuery);
    }
}

const createUserTable = async (client, schema = 'temp') => {
    const query = `
    CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
		CREATE SCHEMA IF NOT EXISTS ${schema};
		CREATE TABLE IF NOT EXISTS ${schema}."users"(
			${getUserEntity(schema)}
            )
            WITH (
                    OIDS = FALSE
            )
            TABLESPACE pg_default;
    `;
    await client.query(query);

}

const createAppointmentTypeTable = async (client, schema = 'temp') => {
    const createQuery = `
		CREATE SCHEMA IF NOT EXISTS ${schema};
		CREATE TABLE IF NOT EXISTS ${schema}."appointment_type"(
			${getAppointmentTypeEntity()}
            )
            WITH (
                    OIDS = FALSE
            )
            TABLESPACE pg_default;
    `;

    //duration will be in sec to match unix time
    const fillTableQuery = `
        INSERT INTO ${schema}."appointment_type" (id, type, duration, description)
        VALUES
            (1,'haircut', 1.5*60,'baliage color'),
            (2,'color', 3*60,'color hair'),
            (3,'ombre',2.5*60,'ombre color'),
            (4,'baliage',4.5*60,'baliage color')
    `;
    
    await client.query(createQuery);
    const result = await client.query(`SELECT COUNT(*) FROM ${schema}."appointment_type"`);
    const rowCount = parseInt(result.rows[0].count);

    if (rowCount === 0) {
      await client.query(fillTableQuery);
    }
}

const createAppointmentTable = async (client, schema = 'temp') => {
    const query = `
    CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
		CREATE SCHEMA IF NOT EXISTS ${schema};
		CREATE TABLE IF NOT EXISTS ${schema}."appointments"(
			${getAppointmentEntity(schema)}
            )
            WITH (
                    OIDS = FALSE
            )
            TABLESPACE pg_default;
    `;
    await client.query(query);

}

const initDb = async (client, schema) => {
	await createUserTypeTable(client, schema);
	await createUserTable(client, schema);
    await createAppointmentTypeTable(client, schema);
	await createAppointmentTable(client, schema); 
};

module.exports = initDb;