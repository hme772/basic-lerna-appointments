
const getAppointmentTypeEntity = () => `id integer NOT NULL,
            type text NOT NULL,
            duration integer NOT NULL,
            description text,
            CONSTRAINT appointment_type_pkey PRIMARY KEY (id)`;

const getAppointmentEntity = (schema) => `id uuid DEFAULT uuid_generate_v4(),
            worker_id integer NOT NULL,
            client_id integer NOT NULL,
            appointment_type integer NOT NULL,
            "creation_time" integer NOT NULL,
            subject text NOT NULL,
            description text,
            "start_time" integer NOT NULL,
            "end_time" integer NOT NULL,
			"is_active" boolean NOT NULL DEFAULT true,
            CONSTRAINT appointments_pkey PRIMARY KEY (id),
            CONSTRAINT id_check CHECK (worker_id >= 100000000 AND worker_id <= 999999999 AND client_id >= 100000000 AND client_id <= 999999999),
            FOREIGN KEY (appointment_type) REFERENCES ${schema}."appointment_type"(id),
            FOREIGN KEY (worker_id) REFERENCES ${schema}."users"(id),
            FOREIGN KEY (client_id) REFERENCES ${schema}."users"(id)`;

module.exports = {getAppointmentTypeEntity, getAppointmentEntity};