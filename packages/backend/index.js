const config = require('./config/config');
const express = require("express");
const bodyParser = require("body-parser");
const {connectPostgres} = require("./DbConfig/DbConnector");
const routers = require('./routes/index');


const app = express();
const PORT = 3000;
//for parsing data
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json({limit: "1mb"})); // 100kb default

const cors = require("cors");
app.use(
    cors({
        origin: "*",
    })
);


app.use(routers);
app.get("/", (req, res) => {
    res.status(200).send("hello from server!");
});

app.listen(PORT, async (error) => {
    if (!error) {
        try {
            await connectPostgres();
        } catch (err) {
            console.error(err);
        }
        console.log(
            "Server is Successfully Running, and App is listening on port " + PORT
        );
    } else {
        console.log("Error occurred, server can't start", error);
    }
});