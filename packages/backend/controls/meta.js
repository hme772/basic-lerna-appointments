const {getAllUserAndAppointmentTypesHandler} = require("../dataAccess/meta");

const getAllUserAndAppointmentTypes = async (req, res) => {
    try {
        const {schema} = req.body;
        const result = await getAllUserAndAppointmentTypesHandler({schema});
        return res.json(result);
    } catch (err) {
        console.log(err);
        res.status(err.status || 500).send(err);
    }

};

module.exports = {getAllUserAndAppointmentTypes};