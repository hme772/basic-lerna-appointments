const {addAppointmentHandler, updateAppointmentHandler, getAllAppointmentsByUserIdHandler, getAllAppointmentsHandler} = require("../dataAccess/appointment");


const getAllAppointments = async(req, res) => {
    try {
        const schema = req.body.schema || 'appointments';
        const result = await getAllAppointmentsHandler({schema});
        return res.json(result);
	} catch (err) {
		console.log(err);
		res.status(err.status || 500).send(err);
	}
    
};

const getAllAppointmentsByUserId = async(req, res) => {
    try {
        const userId = req.params.userId;
        const schema = req.body.schema || 'appointments';
        const result = await getAllAppointmentsByUserIdHandler({schema,userId});
        return res.json(result);
	} catch (err) {
		console.log(err);
		res.status(err.status || 500).send(err);
	}
    
};

const updateAppointment = async(req, res) => {
    try {
        const result = await updateAppointmentHandler(req);
        return res.json({message: `successfully updated ${result}/1 appointments`});
	} catch (err) {
		console.log(err);
		res.status(err.status || 500).send(err);
	}
};

const addAppointment = async (req, res) => {
    try {
        const { schema, worker_id, client_id, appointment_type, subject, start_time, end_time, description } = req.body;
        const result = await addAppointmentHandler({schema, worker_id, client_id, appointment_type, subject, start_time, end_time, description});
        return res.json({message: `successfully added ${result.rowCount}/1 appointments`, ...result});
    } catch (err) {
        console.log(err);
        res.status(err.status || 500).send(err);
    }
};

module.exports = {getAllAppointments, getAllAppointmentsByUserId, updateAppointment, addAppointment};