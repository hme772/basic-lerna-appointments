const {getAppointmentsByTypesHandler, getAppointmentsByTypesForClientsHandler} = require("../dataAccess/graph");

const getAllGraphData = async (req, res) => {
  try {
    const schema = req.body.schema || "appointments";
    const typesGraph = await getAppointmentsByTypesHandler({schema});
    const clientsGraph = await getAppointmentsByTypesForClientsHandler({schema});
    return res.json({clientsGraph,typesGraph})
  } catch (err) {
    console.log(err);
    res.status(err.status || 500).send(err);
  }
};


module.exports = {getAllGraphData};
