import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
    userTypes: [],
    appointmentTypes: [],
    clientsList: [] | undefined, //in ts ?:
    appointmentsList: [] | undefined,
    workersList: [],
    graphData: undefined,
    modal: undefined,
};

export const appointmentsSlice = createSlice({
    name: "appointments",
    initialState,
    reducers: {
        setMetaTypes: (state, action) => {
            state.userTypes = [...action.payload.userTypes];
            state.appointmentTypes = [...action.payload.appointmentTypes];
        },
        setUsers: (state, action) => {
            if (!action.payload) return;
            state.workersList = [...action.payload?.filter((x) => x.user_type === 2)];
            state.clientsList = [...action.payload?.filter((x) => x.user_type === 1)];
        },
        addUser: (state, action) => {
            if (!action.payload) return;
            if (action.payload?.user_type === 2) {
                state.workersList = [...state.workersList, {...action?.payload}];
            }
            else {
                state.clientsList = [...state.clientsList, action.payload];
            }
        },
        updateUser: (state, action) => {
            if (!action.payload) return;
            if (action.payload?.user_type === 2) {
                state.workersList = [...state.workersList.filter((x) => x.id !== action.payload?.id), action.payload];
                state.clientsList = [...state.clientsList.filter((x) => x.id !== action.payload?.id)];
            } else {
                state.workersList = [...state.workersList.filter((x) => x.id !== action.payload?.id)];
                state.clientsList = [...state.clientsList.filter((x) => x.id !== action.payload?.id), action.payload];
            }
        },
        deleteUser: (state, action) => {
            if (!action.payload) return;
            state.workersList = [...state.workersList.filter((x) => x.id !== action.payload)];
            state.clientsList = [...state.clientsList.filter((x) => x.id !== action.payload)];
        },
        setAppointments: (state, action) => {
            state.appointmentsList = action?.payload ? [...action?.payload] : [];
        },
        resetAppointments: (state) => {
            state.appointmentsList = [];
        },
        addAppointment: (state, action) => {
            state.appointmentsList = [...state.appointmentsList, {...action?.payload}];
        },
        removeAppointment: (state, action) => {
            state.appointmentsList = [...state.appointmentsList?.filter(appointment => appointment?.appointment_id !== action?.payload?.appointment_id)];
        },
        updateAppointment: (state, action) => {
            const previousAppointment = state.appointmentsList?.filter(appointment => appointment?.appointment_id === action?.payload?.appointment_id)[0];
            const {client_id, worker_id} = action.payload;
            const client = state.clientsList[state.clientsList.findIndex(client => client.id === client_id)];
            const worker = state.workersList[state.workersList.findIndex(worker => worker.id === worker_id)];
            const filteredAppointments = [...state.appointmentsList?.filter(appointment => appointment?.appointment_id !== action?.payload?.appointment_id)];
            state.appointmentsList = [...filteredAppointments, {
                ...previousAppointment, ...action?.payload,
                worker_first_name: worker.first_name,
                worker_last_name: worker.last_name,
                client_first_name: client.first_name,
                client_last_name: client.last_name
            }];
        },
        setGraphData: (state, action) => {
            const {typesGraph, clientsGraph} = action.payload;
            const clientsMap = new Map();
            for (let i in clientsGraph) {
                if (!clientsMap.has(clientsGraph[i].client_name)) {
                    let typesArray = state.appointmentTypes.map(item => ({type: item.type, count: 0}));
                    let filteredPerClient = clientsGraph.filter(item => item.client_name == clientsGraph[i].client_name);
                    filteredPerClient.forEach(item => {
                        typesArray[typesArray.findIndex(x => x.type == item.type)].count = parseInt(item.count);
                    });
                    clientsMap.set(clientsGraph[i].client_name, typesArray);
                }
            }
            const clientsGraphData = [];
            for (let i in state.appointmentTypes) {
                let dataPerType = [];
                for (let [_, value] of clientsMap) {
                    dataPerType.push(value[i].count)
                }
                clientsGraphData.push({
                    label: state.appointmentTypes[i].type, data: dataPerType
                })
            }
            state.graphData = {
                types: {
                    labels: typesGraph.map(item => item.type),
                    data: typesGraph.map(item => item.count)
                }, clients: {labels: Array.from(clientsMap.keys()), data: clientsGraphData}
            };
        },
        setModalData: (state, action) => {
            state.modal = action.payload;
        },
        reset: (state) => {
            state.clientsList = undefined;
            state.appointmentsList = undefined;
            state.workersList = [];
            state.graphData = undefined;
            state.module = undefined;
        },
    },
});

// Action creators are generated for each case reducer function
export const {
    setUsers,
    addUser,
    updateUser,
    deleteUser,
    setMetaTypes,
    setAppointments,
    resetAppointments,
    addAppointment,
    removeAppointment,
    updateAppointment,
    setGraphData,
    setModalData,
    reset,
} = appointmentsSlice.actions;

export default appointmentsSlice.reducer;
