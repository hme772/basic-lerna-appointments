import {createSelector} from "@reduxjs/toolkit";

const getAppointmentsState = (state) => state.appointments;
export const getUserTypes = createSelector(
    [getAppointmentsState],
    (appointments) => {
        const userTypes = [...appointments?.userTypes];
        return userTypes.sort((a, b) => {
            return a.id - b.id;
        });
    }
);

export const getAppointmentTypes = createSelector(
    [getAppointmentsState],
    (appointments) => {
        const appointmentTypes = [
            ...appointments?.appointmentTypes.map((type) => ({
                ...type,
                text: type.type,
                value: type.type,
            })),
        ];
        return appointmentTypes?.sort((a, b) => {
            return a.id - b.id;
        });
    }
);

export const getClients = createSelector(
    [getAppointmentsState],
    (appointments) => {
        if (!appointments?.clientsList) return [];
        const clients = [
            ...[...appointments?.clientsList]?.map((client) => ({
                ...client,
                text: `${client.first_name} ${client.last_name}`,
                value: client.id,
            })),
        ];
        return clients?.sort((a, b) => {
            return a.id - b.id;
        });
    }
);

export const getWorkers = createSelector(
    [getAppointmentsState],
    (appointments) => {
        const workers = [
            ...appointments?.workersList?.map((worker) => ({
                ...worker,
                text: `${worker.first_name} ${worker.last_name}`,
                value: worker.id,
            })),
        ];
        return workers?.sort((a, b) => {
            return a.id - b.id;
        });
    }
);

export const getAllAppointments = createSelector(
    [getAppointmentsState],
    (appointments) => {
        if (!appointments?.appointmentsList) return [];
        const userAppointments = [...appointments?.appointmentsList];
        return userAppointments?.sort((a, b) => {
            return new Date(b.start_time) - new Date(a.start_time);
        });
    }
);

export const getUserAppointments = (id) => createSelector(
    [getAllAppointments],
    (appointments) => {
        if (!appointments || appointments.length === 0) return [];
        return appointments?.filter(appointment => appointment.client_id == id || appointment.worker_id == id);
    }
);

export const getWorkerAppointmentsFromTodaySorted = (id) => createSelector(
    [getUserAppointments(id)],
    (appointments) => {
        if (!appointments) return [];
        const now = new Date(Date.now());
        const todayDate = new Date(
            now.getFullYear(),
            now.getMonth(),
            now.getDate()
        );
        const userAppointments = [
            ...appointments?.filter(
                (appointment) => new Date(appointment.start_time) >= todayDate
            ),
        ];
        return userAppointments?.sort((a, b) => {
            return new Date(a.start_time) - new Date(b.start_time);
        });
    }
);

const convertAppointmentsToCalendarAppointments = (appointments) => {
    if (!appointments) return [];
    const calendarAppointments = [...appointments];
    if (calendarAppointments.length === 0) return [];
    return calendarAppointments?.map((appointment) => ({
        Id: appointment.appointment_id,
        Subject: `${appointment.worker_first_name} ${appointment.worker_last_name} ${appointment.type} appointment with ${appointment.client_first_name} ${appointment.client_last_name}`,
        StartTime: new Date(appointment.start_time),
        EndTime: new Date(appointment.end_time),
        EventType: appointment.type,
        Description: appointment.description,
        Client: appointment.client_id,
        Worker: appointment.worker_id
    }));
}

export const getAllAppointmentsForCalendar = createSelector(
    [getAllAppointments],
    (appointments) => convertAppointmentsToCalendarAppointments(appointments)
);

export const getUserAppointmentsForCalendar = (id) => createSelector(
    [getUserAppointments(id)],
    (appointments) => convertAppointmentsToCalendarAppointments(appointments)
);

export const getGraphData = createSelector([getAppointmentsState], (appointments) => appointments?.graphData);
export const getAppointmentsByTypeGraphData = createSelector([getGraphData], (graphs) => graphs?.types);
export const getAppointmentsPerClientByTypeGraphData = createSelector([getGraphData], (graphs) => graphs?.clients);
export const getCurrentUser = (id) => createSelector([getAppointmentsState], (appointments) => {
    const allUsers = [...appointments?.workersList, ...appointments?.clientsList];
    if (!id || allUsers.length === 0) return {};
    return allUsers.find(user => user.id == id);
});

export const getModalData = createSelector([getAppointmentsState], (appointments) => appointments?.modal);
export const getIsWorkerUser = (userTypeId) => createSelector([getUserTypes], (userTypes) => userTypes
    ?.find((type) => type.id === userTypeId)
    ?.type.trim()
    .toLowerCase() === "worker"
);

export const getWorkersWithoutConnected = (connectedUserId) => createSelector([getWorkers], (workers) => {
    return workers?.filter(worker => worker.value != connectedUserId);
});
export const getClientsWithoutConnected = (connectedUserId) => createSelector([getClients], (clients) => {
    return clients?.filter(client => client.value != connectedUserId);
});