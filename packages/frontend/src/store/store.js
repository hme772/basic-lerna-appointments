import { configureStore } from '@reduxjs/toolkit'
import appointmentsReducer from './reducers/appointments-reducer';

export const slicedStore = configureStore({
    reducer: {
      appointments: appointmentsReducer
    },
  });
  
export default slicedStore;