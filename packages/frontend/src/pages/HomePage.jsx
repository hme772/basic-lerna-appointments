import StartingPageContent from '../components/StartingPage/StartingPageContent';
import AppointmentList from '../components/AppointmentList/AppointmentList';

const HomePage = () => {
  return <>
  <StartingPageContent />
  <AppointmentList/>
  </>;
};

export default HomePage;
