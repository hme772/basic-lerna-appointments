import React from "react";
import {
  Chart as ChartJS,
  ArcElement,
  BarElement,
  CategoryScale,
  LinearScale,
  Tooltip,
  Legend,
} from "chart.js";
import { Bar, Doughnut } from "react-chartjs-2";
import Card from "../components/UI/Card";
import classes from "./ReportPage.module.css";
import { useSelector } from "react-redux";
import { getAppointmentsByTypeGraphData, getAppointmentsPerClientByTypeGraphData } from '../store/selectors/appointments-selector';
import { evnObj } from "../services/env";

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  ArcElement,
  Tooltip,
  Legend
);



export const optionsBar = {
  plugins: {
    title: {
      display: true,
      text: "Chart.js Bar Chart - Stacked",
    },
  },
  responsive: true,
  scales: {
    x: {
      stacked: true,
    },
    y: {
      stacked: true,
    },
  },
};


function ReportPage() {
  const typeGraphData = useSelector(getAppointmentsByTypeGraphData);
  const typesData = !typeGraphData ? undefined : {
      labels: typeGraphData.labels,
      datasets: [
        {
          label: "# of appointments",
          data: typeGraphData.data,
          backgroundColor: evnObj.typeGraphColors,
          borderColor: evnObj.typeGraphColors,
          borderWidth: 1,
        },
      ],
    };

  const clientsGraphData = useSelector(getAppointmentsPerClientByTypeGraphData);
  const dataBar = !clientsGraphData ? undefined : {
    labels: clientsGraphData.labels,
    datasets: clientsGraphData.data.map((item,index)=>({...item,backgroundColor:evnObj.typeGraphColors[index]})),
  };
  return (
    <div className={classes.report}>
      <Card>
        <h1>Reports</h1>
        <label htmlFor="id">Appointments By Types</label>
        <div className={classes.chart}>
          {typeGraphData ?
              <Doughnut data={typesData} />
              : <p>No data found for this chart!</p>}

        </div>

        <label htmlFor="barChart">Appointments Per Client By Type</label>
        <div className={classes.chart}>
          {clientsGraphData ?
              <Bar options={optionsBar} data={dataBar} />
              : <p>No data found for this chart!</p>}
        </div>
      </Card>
    </div>
  );
}
export default ReportPage;
