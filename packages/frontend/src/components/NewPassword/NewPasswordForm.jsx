import { useRef, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import AuthContext from '../../store/auth-context/auth-context';
import classes from './NewPasswordForm.module.css';

const NewPasswordForm = () => {
  const navigate = useNavigate();
  const newPasswordRef = useRef();
  const authCtx = useContext(AuthContext);

  const submit = event => {
    event.preventDefault();
    const newPassword = newPasswordRef.current.value;
    
    fetch("https://identitytoolkit.googleapis.com/v1/accounts:update?key=AIzaSyBZhBpreJp4NNp6nZWkgrkF1AHWR1CB12s", {
      method: "POST",
      body: JSON.stringify({
        idToken: authCtx.token,
        password: newPassword,
        returnSecureToken: false,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    }).then(res => {
      console.log("passwrd changed!");
      navigate('/',{replace: true});
    });
  }

  return (
    <form className={classes.form} onSubmit={submit}>
      <div className={classes.control}>
        <label htmlFor='new-password'>New Password</label>
        <input type='password' id='new-password' minLength={6} ref={newPasswordRef} required/>
      </div>
      <div className={classes.action}>
        <button>Change Password</button>
      </div>
    </form>
  );
}

export default NewPasswordForm;
