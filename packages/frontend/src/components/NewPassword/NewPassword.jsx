import NewPasswordForm from './NewPasswordForm';
import classes from './NewPassword.module.css';

const NewPassword = () => {
  return (
    <section className={classes.newPassword}>
      <h1>Your New Password</h1>
      <NewPasswordForm />
    </section>
  );
};

export default NewPassword;
