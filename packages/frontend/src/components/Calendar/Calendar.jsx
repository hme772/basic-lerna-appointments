import {
  ScheduleComponent,
  Day,
  Week,
  WorkWeek,
  Month,
  Agenda,
  Inject,
  DragAndDrop,
  Resize,
  MonthAgenda,
  TimelineViews,
  TimelineMonth,
} from "@syncfusion/ej2-react-schedule";
import * as React from "react";
import {useState, useEffect} from "react";
import { registerLicense } from "@syncfusion/ej2-base";
import BottomNavigation from "@mui/material/BottomNavigation";
import BottomNavigationAction from "@mui/material/BottomNavigationAction";
import Card from "../UI/Card";
import classes from "./Calendar.module.css";
import { createElement } from "@syncfusion/ej2-base";
import { DropDownList } from "@syncfusion/ej2-dropdowns";
import { formatDate, getMovedTimeMS } from "../../services/time";
import { useSelector, useDispatch } from "react-redux";
import {
  getAppointmentTypes,
  getClients,
  getWorkers,
  getAllAppointmentsForCalendar,
  getUserAppointmentsForCalendar,
} from "../../store/selectors/appointments-selector";
import {
  addAppointmentToDb,
  updateAppointmentInDb,
  deleteAppointmentToDb,
} from "../../services/server/appointment";
import {getGraphData} from "../../services/server/graph";
import {
  addAppointment,
  updateAppointment,
  removeAppointment,
  setGraphData,
} from "../../store/reducers/appointments-reducer";
import LoadingSpinner from "../UI/LoadingSpinner";
import Button from "@mui/material/Button"
import ArrowBackIosRounded from '@mui/icons-material/ArrowBackIosRounded';
import ArrowForwardIosRounded from '@mui/icons-material/ArrowForwardIosRounded';
import Switch from '@mui/material/Switch';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';


registerLicense(
  "Ngo9BigBOggjHTQxAR8/V1NBaF5cXmZCekx3Qnxbf1x0ZFxMYFtbQHVPMyBoS35RckVgWHdeeXRSQmVfUkFz"
);


const Calendar = ({token, currentUserId}) => {
  const [endDate, setEndDate] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [isAllSchedule, setIsAllSchedule] = useState(true);
  const dispatch = useDispatch();
  const eventTypes = useSelector(getAppointmentTypes);
  const clients = useSelector(getClients);
  const workers = useSelector(getWorkers);
  const data = useSelector(isAllSchedule ? getAllAppointmentsForCalendar : getUserAppointmentsForCalendar(currentUserId));
  const [view, setView] = useState("Week");
  const [schedulerDate, setSchedulerDate] = useState(new Date(Date.now())); // State to manage scheduler date

  const shouldUseEndDate = (eventData) => {
    const isEndDateRelevant = endDate !== null && !isNaN(new Date(endDate).getTime());
    const isEventTypeDefined = eventData?.EventType;
    const isMismatchedDates = endDate != eventData?.EndTime;
    const isEndTimeMatchDuration = eventData?.EndTime.getTime() - eventTypes.find(type=> type.type === eventData?.EventType).duration*60000 == eventData?.StartTime.getTime();
    return isEndDateRelevant &&
    isEventTypeDefined &&
        isMismatchedDates &&
        !isEndTimeMatchDuration;
  }

  const onActionBegin = (args) => {
    console.log(args);
    const eventData = args.data || [];
    const eventObject = {
      Subject: eventData[0]?.Subject,
      StartTime: Math.floor(new Date(eventData[0]?.StartTime).getTime() / 1000),
      EndTime:
        endDate !== null &&
        eventData[0]?.EventType &&
        endDate != eventData[0]?.EndTime
          ? Math.floor(new Date(endDate).getTime() / 1000)
          : Math.floor(new Date(eventData[0]?.EndTime).getTime() / 1000),
      Client: eventData[0]?.Client,
      Worker: eventData[0]?.Worker,
      EventType: eventData[0]?.EventType,
      Duration: eventData[0]?.duration,
      IsAllDay: eventData[0]?.IsAllDay,
      Description: eventData[0]?.Description,
      Location: eventData[0]?.Location,
      RecurrenceRule: eventData[0]?.RecurrenceRule,
      RecurrenceID: eventData[0]?.RecurrenceID,
      RecurrenceException: eventData[0]?.RecurrenceException,
      Id: eventData[0]?.Id,
      CategoryColor: eventData[0]?.CategoryColor,
      StartTimezone: eventData[0]?.StartTimezone,
      EndTimezone: eventData[0]?.EndTimezone,
      IsReadonly: eventData[0]?.IsReadonly,
      IsBlock: eventData[0]?.IsBlock,
    };

    switch (args.requestType) {
      case "eventCreate":
        setIsLoading(true);
        //some function
        let addDbObj = {
          worker_id: eventObject.Worker,
          client_id: eventObject.Client,
          appointment_type:
            eventTypes[
              eventTypes.findIndex((type) => type.type == eventObject.EventType)
            ].id,
          subject: eventObject.Subject,
          start_time: eventObject.StartTime,
          end_time: eventObject.EndTime,
          description: eventObject.Description,
        };
        addAppointmentToDb({token,...addDbObj})
          .then((res) => {
            let worker = workers.filter(
              (worker) => worker.id == eventObject.Worker
            )[0];
            let client = clients.filter(
              (client) => client.id == eventObject.Client
            )[0];
            let storeObj = {
              worker_id: eventObject.Worker,
              client_id: eventObject.Client,
              worker_first_name: worker.first_name,
              worker_last_name: worker.last_name,
              client_first_name: client.first_name,
              client_last_name: client.last_name,
              description: eventObject.Description,
              appointment_id: res.data.id,
              type: eventObject.EventType,
              is_active: true,
              subject: eventObject.Subject,
              start_time: new Date(eventObject.StartTime * 1000).toISOString(),
              end_time: new Date(eventObject.EndTime * 1000).toISOString(),
            };

            dispatch(addAppointment(storeObj));
          })
          .catch((err) => {
            alert(err.message);
            setIsLoading(false);
          });
        setEndDate(null);
        console.log("add", eventData);
        break;
      case "eventChange":
      case "eventResize":
      case "eventMove":
        setIsLoading(true);
        let editDbObj = {
          id: eventData?.Id,
          appointment_type:
            eventTypes[
              eventTypes.findIndex((type) => type.type == eventData.EventType)
            ]?.id,
          client_id: eventData.Client,
          worker_id: eventData.Worker,
          subject: eventData.Subject,
          start_time: Math.floor(
            new Date(eventData?.StartTime).getTime() / 1000
          ),
          end_time:
          shouldUseEndDate(eventData)
              ? Math.floor(new Date(endDate).getTime() / 1000)
              : Math.floor(new Date(eventData?.EndTime).getTime() / 1000),
          description: eventData?.Description,
        };
        updateAppointmentInDb({token,...editDbObj})
          .then((res) => {
            delete editDbObj.appointment_type;
            delete editDbObj.id;
            dispatch(
              updateAppointment({
                ...editDbObj,
                subject: eventData.Subject,
                appointment_id: eventData?.Id,
                start_time: new Date(editDbObj.start_time * 1000).toISOString(),
                end_time: new Date(editDbObj.end_time * 1000).toISOString(),
                is_active: true,
                type: eventData.EventType,
              })
            );
          })
          .catch((err) => {
            alert(err.message);
            console.log(err)
            setIsLoading(false);
          });
        console.log("update", editDbObj);
        break;
      case "eventRemove":
        setIsLoading(true);
        deleteAppointmentToDb({token, id:eventObject.Id})
          .then((res) =>
            dispatch(removeAppointment({ appointment_id: eventObject.Id }))
          )
          .catch((err) => {
            alert(err.message);
            setIsLoading(false);
          });
        console.log("delete", eventData);
        break;
      default:
        break;
    }
    const requestTypes = [
      "eventCreate",
      "eventChange",
      "eventResize",
      "eventMove",
    ];
    const shouldRefreshGraphs = requestTypes.includes(args.requestType);
    if (shouldRefreshGraphs) {
      getGraphData(token)
        .then((res) => dispatch(setGraphData(res.data)))
        .catch((err) => {
          alert(err.message);
          setIsLoading(false);
        });
    }
    setIsLoading(false);
  };

  const onPopupOpen = (args) => {
    if (args.type === "Editor") {
      if (!args.element.querySelector(".custom-field-row")) {
        let row = createElement("div", { className: "custom-field-row" });
        let formElement = args.element.querySelector(".e-schedule-form");
        formElement.firstChild.insertBefore(
          row,
          formElement.firstChild.firstChild
        );
        let container = createElement("div", {
          className: "custom-field-container",
        });
        let inputEle = createElement("input", {
          className: "e-field",
          attrs: { name: "EventType" },
        });
        container.appendChild(inputEle);
        row.appendChild(container);
        let drowDownList = new DropDownList({
          dataSource: eventTypes,
          fields: { text: "text", value: "value" },
          value: args.data.EventType,
          floatLabelType: "Always",
          placeholder: "Event Type",
          change: (e) => {
            console.log("in change", args);
            const selectedEventType = e.itemData;
            const durationInMinutes = selectedEventType?.duration || 0;
            const startTime = new Date(
              args.element.querySelector(".e-start").value
            );
            // Convert minutes to milliseconds
            const endTime = new Date(
              startTime.getTime() + durationInMinutes * 60000
            );
            args.duration = durationInMinutes;
            args.data.EndTime = endTime;
            setEndDate(endTime);
            args.data.EventType = e.value;
            const currentClient = clients.findIndex(
              (client) => client.id == args.data.Client
            );
            const subjectInput = args.element.querySelector(".e-subject");
            if (subjectInput) {
              subjectInput.value = `${args.data.EventType} appointment with ${
                currentClient !== -1 && clients[currentClient]
                  ? `${clients[currentClient].first_name} ${clients[currentClient].last_name}`
                  : ""
              }`;
            }
            args.data.Subject = subjectInput.value;

            const descriptionInput =
              args.element.querySelector(".e-description");
            if (descriptionInput) {
              descriptionInput.value = selectedEventType?.description;
            }
            args.data.Description = descriptionInput.value;

            // Format the end time and update the EndTime input element value
            const endTimeInput = args.element.querySelector(".e-end");
            if (endTimeInput) {
              endTimeInput.value = formatDate(endTime);
            }

            // Update the EventType input element value
            const eventTypeElement =
              args.element.querySelector('[name="EventType"]');
            if (eventTypeElement) {
              eventTypeElement.value = selectedEventType?.value;
            }
          },
        });
        drowDownList.appendTo(inputEle);
        inputEle.setAttribute("name", "EventType");

        //client

        let clientInputEle = createElement("input", {
          className: "e-field",
          attrs: { name: "Client" },
        });
        container.appendChild(clientInputEle);
        row.appendChild(container);
        let clientDrowDownList = new DropDownList({
          dataSource: clients,
          fields: { text: "text", value: "value" },
          value: args.data.Client,
          floatLabelType: "Always",
          placeholder: "Client",
          change: (e) => {
            console.log("in client change", args);
            const selectedClient = e.itemData;
            args.data.Client = e.value;
            const currentClient = clients.findIndex(
              (client) => client.id == args.data.Client
            );
            const subjectInput = args.element.querySelector(".e-subject");
            if (subjectInput) {
              subjectInput.value = `${args.data.EventType} appointment with ${
                currentClient !== -1 && clients[currentClient]
                  ? `${clients[currentClient].first_name} ${clients[currentClient].last_name}`
                  : ""
              }`;
            }
            args.data.Subject = subjectInput.value;

            // Update the client input element value
            const clientElement = args.element.querySelector('[name="Client"]');
            if (clientElement) {
              clientElement.value = selectedClient?.value;
            }
          },
        });
        clientDrowDownList.appendTo(clientInputEle);
        clientInputEle.setAttribute("name", "Client");

        //worker
        let workerInputEle = createElement("input", {
          className: "e-field",
          attrs: { name: "Worker" },
        });
        container.appendChild(workerInputEle);
        row.appendChild(container);
        let workerDrowDownList = new DropDownList({
          dataSource: workers,
          fields: { text: "text", value: "value" },
          value: args.data.Worker,
          floatLabelType: "Always",
          placeholder: "Worker",
          change: (e) => {
            console.log("in worker change", args);
            const selectedWorker = e.itemData;
            args.data.Worker = e.value;
            // Update the client input element value
            const workerElement = args.element.querySelector('[name="Worker"]');
            if (workerElement) {
              workerElement.value = selectedWorker?.value;
            }
          },
        });
        workerDrowDownList.appendTo(workerInputEle);
        workerInputEle.setAttribute("name", "Worker");
      }
    }
  };

  const onViewChange = (event, newValue) => {
    console.log(newValue);
    setView(newValue);
  };

  useEffect(() => {
    const toolbarContainer = document.querySelector(
      ".e-schedule-toolbar-container"
    );
    if (
      toolbarContainer &&
      toolbarContainer?.parentNode &&
      toolbarContainer?.parentNode?.contains(toolbarContainer)
    ) {
      toolbarContainer.style.display = "none";
    }
    const durationContainer = document.querySelector(
      ".e-all-day-time-zone-row"
    );
    if (
      durationContainer &&
      durationContainer?.parentNode &&
      durationContainer?.parentNode?.contains(durationContainer)
    ) {
      durationContainer.style.display = "none";
    }
    const recurrenceContainer = document.querySelector(
      ".e-recurrenceeditor .e-editor"
    );
    if (
      recurrenceContainer &&
      recurrenceContainer?.parentNode &&
      recurrenceContainer?.parentNode?.contains(recurrenceContainer)
    ) {
      recurrenceContainer.style.display = "none";
    }
    const locationAndSubjectContainer = document.querySelector(
      ".e-title-location-row"
    );
    if (
      locationAndSubjectContainer &&
      locationAndSubjectContainer?.parentNode &&
      locationAndSubjectContainer?.parentNode?.contains(
        locationAndSubjectContainer
      )
    ) {
      locationAndSubjectContainer.style.display = "none";
    }
  }, []);

  const onBackClick = () => {
    const backedTimeMS = getMovedTimeMS({currentTime:schedulerDate,view});
    setSchedulerDate(new Date(backedTimeMS));
  }
  const onForwardClick = () => {
    const fowardedTimeMS = getMovedTimeMS({currentTime:schedulerDate,view,isForward:true});
    setSchedulerDate(new Date(fowardedTimeMS));
  }
  const resetCurrentTime = ()=>setSchedulerDate(new Date(Date.now()));

  const toggleSchedules = () => {
    setIsAllSchedule(prev => !prev);
  }
  return (
    <div className={classes.calendar}>
      <Card>
        <h1>{`${isAllSchedule ? "All Workers": "My"} Schedule`}</h1>
        <FormGroup className={classes.scheduleSwitch}>
          <FormControlLabel control={<Switch checked={isAllSchedule}
                                             onChange={toggleSchedules}
                                             inputProps={{ 'aria-label': 'change schedules' }}
                                             color="secondary" />} label={`Show ${isAllSchedule ? "all workers": "only my"} schedule`}/>
        </FormGroup>
        <div className={classes.navigationButtons}>
          <Button sx={{
              fontWeight: "bold",
              color: "#873abb",
              width: 50,
              border: "2px solid #873abb",
              borderRadius: "5px",
              backgroundColor: "#f1e1fc",
              "& .MuiButton-label": {
                fontSize: "20px",
                fontWeight: "bold",
                color: "#9f5ccc",
              },
              "& .Mui-selected": {
                color: "#38015c",
                fontSize: "32px !important",
                fontWeight: "bold",
              },
            }} onClick={onBackClick}><ArrowBackIosRounded/></Button>
        <div className={classes.timeNavigation}>
          <BottomNavigation
            sx={{
              fontWeight: "bold",
              color: "#873abb",
              width: 500,
              border: "2px solid #873abb",
              borderRadius: "5px",
              backgroundColor: "#f1e1fc",
              "& .MuiBottomNavigationAction-label": {
                fontSize: "20px",
                fontWeight: "bold",
                color: "#9f5ccc",
              },
              "& .Mui-selected": {
                color: "#38015c",
                fontSize: "32px !important",
                fontWeight: "bold",
              },
            }}
            showLabels
            value={view}
            onChange={onViewChange}
          >
            <BottomNavigationAction label="Day" value="Day" />
            <BottomNavigationAction label="Week" value="Week" />
            <BottomNavigationAction label="Month" value="Month" />
          </BottomNavigation>
        </div>
        <Button sx={{
              fontWeight: "bold",
              color: "#873abb",
              width: 50,
              border: "2px solid #873abb",
              borderRadius: "5px",
              backgroundColor: "#f1e1fc",
              "& .MuiButton-label": {
                fontSize: "20px",
                fontWeight: "bold",
                color: "#9f5ccc",
              },
              "& .Mui-selected": {
                color: "#38015c",
                fontSize: "32px !important",
                fontWeight: "bold",
              },
            }} onClick={onForwardClick}><ArrowForwardIosRounded/></Button>
        </div>
        <div className={classes.navigationButtons}>
          <div className={classes.timeNavigation}>

        <Button sx={{
              fontWeight: "bold",
              color: "#873abb",
              width: 500,
              border: "2px solid #873abb",
              borderRadius: "5px",
              backgroundColor: "#f1e1fc",
              "& .MuiButton-label": {
                fontSize: "20px",
                fontWeight: "bold",
                color: "#9f5ccc",
              },
              "& .Mui-selected": {
                color: "#38015c",
                fontSize: "32px !important",
                fontWeight: "bold",
              },
            }} onClick={resetCurrentTime}>Return To Current Date</Button>
          </div>
        </div>
        {isLoading && (
          <div className="centered">
            <LoadingSpinner />
          </div>
        )}
        <ScheduleComponent
          width="80%"
          height="650px"
          style={{
            display: "flex",
            alignItems: "center",
            margin: "3rem auto",
            
            borderRadius: "6px",
            backgroundColor: "#38015c",
            boxShadow: "0 1px 4px rgba(0, 0, 0, 0.2)",
            padding: "1rem",
            textAlign: "center",
            }}
          currentView={view}
          selectedDate={schedulerDate}
          popupOpen={onPopupOpen}
          actionBegin={onActionBegin}
          toolbarItems={[
            "Add",
            "Edit",
            "Delete",
            "Update",
            "Today",
            "Day",
            "Week",
            "WorkWeek",
            "Month",
            "Agenda",
          ]}
          eventSettings={{
            dataSource: data,
          }}
        >
          <Inject
            services={[
              Day,
              Week,
              WorkWeek,
              Month,
              Agenda,
              MonthAgenda,
              TimelineViews,
              TimelineMonth,
              DragAndDrop,
              Resize,
            ]}
          />
        </ScheduleComponent>
      </Card>
    </div>
  );
};
export default Calendar;
