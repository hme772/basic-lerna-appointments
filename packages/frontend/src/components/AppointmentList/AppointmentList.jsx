import { List } from "@mui/material";
import AppointmentItem from "./AppointmentItem";
import classes from './AppointmentList.module.css';
import { useSelector } from "react-redux";
import {getUserAppointments, getWorkerAppointmentsFromTodaySorted} from "../../store/selectors/appointments-selector";
import { useContext } from "react";
import AuthContext from "../../store/auth-context/auth-context";
const AppointmentList= () => {
    const authCtx = useContext(AuthContext);
    const id = authCtx.id;
    const isWorker = authCtx.getIsWorkerUser();
    const appointmentsList = useSelector(isWorker ? getWorkerAppointmentsFromTodaySorted(id) : getUserAppointments(id));

    return <List aria-labelledby="decorated-list-demo" sx={{margin: '3rem auto',
        width: '100%',
        maxWidth: '25rem',
        borderRadius: '6px',
        backgroundColor: '#38015c',
        boxShadow: '0 1px 4px rgba(0, 0, 0, 0.2)',
        padding: '1rem',
        textAlign: 'center'}} className={classes.appointmentList}>
               <h1> My Appointments:</h1>
            {appointmentsList?.map(appointment => <AppointmentItem key={appointment.appointment_id} isWorker={isWorker} {...appointment} />)}
        </List>
   
};

export default AppointmentList;