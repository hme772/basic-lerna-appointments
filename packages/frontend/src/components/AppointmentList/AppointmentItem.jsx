import { ListItem, ListItemText, Typography, Paper  } from "@mui/material";
import { Fragment } from "react";
import {formatDateForList} from "../../services/time"

const AppointmentItem= ({appointment_id, worker_first_name, worker_last_name, client_first_name, client_last_name, type, description, isWorker,start_time,end_time}) => {
    return <ListItem key={appointment_id}  alignItems="flex-start" sx={{color: "white"}}  >
      <Paper elevation={3} style={{ color: "white",backgroundColor: '#9f5ccc',borderRadius: '20px', padding: '10px', width: '100%' }}>
        <ListItemText primary={<span style={{ fontWeight: 'bold' }}>{type} at {formatDateForList(start_time, end_time)}</span>}
        secondary={
            <Fragment>
              <Typography
                sx={{ display: 'flex' }}
                component="span"
                variant="body2"
                color="text.primary"
              >
                {isWorker ? `${type} appointment with ${client_first_name} ${client_last_name}` : `${type} appointment with ${worker_first_name} ${worker_last_name}`}
              </Typography>
              {`Description: ${description}`}
            </Fragment>
          }

        />
        </Paper>
    </ListItem>
};

export default AppointmentItem;