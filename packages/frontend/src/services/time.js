export const formatDate = (dateString) => {
  const date = new Date(dateString);
  const month = (date.getMonth() + 1).toString().padStart(2, "0");
  const day = date.getDate().toString().padStart(2, "0");
  const year = date.getFullYear() % 100; // Get the last two digits of the year
  let hours = date.getHours();
  if (hours > 12) {
    hours -= 12;
  } else if (hours === 0) {
    hours = 12; // 0 hour corresponds to 12 AM
  }
  hours = hours.toString().padStart(2, "0");
  const minutes = date.getMinutes().toString().padStart(2, "0");
  const amOrPm = date.getHours() >= 12 ? "PM" : "AM";
  const formattedDate = `${month}/${day}/${year} ${hours}:${minutes} ${amOrPm}`;
  return formattedDate;
};
export const formatDateForList = (startDateString, endDateString) => {
  const startDate = new Date(startDateString), endDate = new Date(endDateString);
  const month = (startDate.getMonth() + 1).toString().padStart(2, "0");
  const day = startDate.getDate().toString().padStart(2, "0");
  const year = startDate.getFullYear() % 100; // Get the last two digits of the year
  let startHours = startDate.getHours();
  if (startHours > 12) {
    startHours -= 12;
  } else if (startHours === 0) {
    startHours = 12; // 0 hour corresponds to 12 AM
  }
  startHours = startHours.toString().padStart(2, "0");
  const startMinutes = startDate.getMinutes().toString().padStart(2, "0");
  const startAmOrPm = startDate.getHours() >= 12 ? "PM" : "AM";
  let endHours = endDate.getHours();
  if (endHours > 12) {
    endHours -= 12;
  } else if (endHours === 0) {
    endHours = 12; // 0 hour corresponds to 12 AM
  }
  endHours = endHours.toString().padStart(2, "0");
  const endMinutes = endDate.getMinutes().toString().padStart(2, "0");
  const endAmOrPm = endDate.getHours() >= 12 ? "PM" : "AM";
  const formattedDate = `${month}/${day}/${year} ${startHours}:${startMinutes} ${startAmOrPm} - ${endHours}:${endMinutes} ${endAmOrPm}`;
  return formattedDate;
};

const timeOptionsMS ={
  Day:24*60*60*1000,
  Week:7*24*60*60*1000,
  Month:30*24*60*60*1000
}

export const getMovedTimeMS = ({currentTime,view,isForward}) => {
  const currentTimeMs = currentTime.getTime();
  const timeToMove = timeOptionsMS[view];
  return isForward ? currentTimeMs + timeToMove : currentTime - timeToMove;
}