import axiosInstance, {getHeaders, routes} from "../axiosConfig";

export const getGraphData = async (token) => {
    try {
        return await axiosInstance.get(`${routes.graph}`, {headers: getHeaders(token)});
    } catch (e) {
        return false;
    }
};