import axiosInstance, {getHeaders, routes} from "../axiosConfig";

export const getAllAppointments = async (token) => {
    try {
        return await axiosInstance.get(routes.appointment, {headers: getHeaders(token)});
    } catch (e) {
        return false;
    }
};

export const getAppointmentsById = async ({id, token}) => {
    try {
        if (id.trim() === "") return;
        return await axiosInstance.get(`${routes.appointment}${id}`, {headers: getHeaders(token)});
    } catch (e) {
        return false;
    }
};

export const addAppointmentToDb = async ({
                                             token,
                                             worker_id,
                                             client_id,
                                             appointment_type,
                                             subject,
                                             start_time,
                                             end_time,
                                             description,
                                         }) => {
    try {
        if (
            !worker_id ||
            !client_id ||
            !appointment_type ||
            !start_time ||
            !end_time
        )
            return;
        return await axiosInstance.post(
            routes.appointment,
            {
                schema: "appointments",
                worker_id,
                client_id,
                appointment_type,
                subject,
                start_time,
                end_time,
                description,
            },
            {headers: getHeaders(token)}
        );
    } catch (e) {
        throw e;
    }
};

export const updateAppointmentInDb = async ({
                                                token, id,
                                                worker_id,
                                                client_id,
                                                appointment_type,
                                                subject,
                                                start_time,
                                                end_time,
                                                description,
                                                is_active
                                            }) => {
    try {
        if (id?.trim() === "") return;
        if (
            !worker_id &&
            !client_id &&
            !appointment_type &&
            !start_time &&
            !end_time &&
            !description &&
            is_active === undefined
        )
            return;
        return await axiosInstance.put(
            `${routes.appointment}${id}`,
            {
                schema: "appointments",
                worker_id,
                client_id,
                appointment_type,
                subject,
                start_time,
                end_time,
                description,
                is_active
            },
            {headers: getHeaders(token)}
        );
    } catch (e) {
        throw e;
    }
};

export const deleteAppointmentToDb = async ({token, id}) => {
    try {
        if (id.trim() === "") return;
        return await updateAppointmentInDb(
            {
                id,
                token,
                is_active: false
            }
        );
    } catch (e) {
        throw e;
    }
};