import {getAllMetaTypes} from "./server/meta";
import {getAllAppointments} from "./server/appointment";
import {getAllUsers} from "./server/user";
import {getGraphData} from "./server/graph";
import {slicedStore} from "../store/store";
import {setUsers, setMetaTypes, setGraphData, setAppointments, reset} from '../store/reducers/appointments-reducer';

export const setInitializedData = async (token) => {
    const metaTypes = await getAllMetaTypes().then(d => d.data);
    slicedStore.dispatch(setMetaTypes(metaTypes));
};

export const setAppData = ({token, id}) => {
    getAllAppointments(token).then((appointments) =>
        slicedStore.dispatch(setAppointments(appointments.data))
    );
    getAllUsers(token).then(users => slicedStore.dispatch(setUsers(users.data)));
    getGraphData(token).then(graphData => slicedStore.dispatch(setGraphData(graphData.data)));
};

export const resetAppData = () => {
    slicedStore.dispatch(reset());
};